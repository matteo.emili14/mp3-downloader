const electron = require('electron');
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const settings = require('electron-settings');
const BrowserWindow = electron.BrowserWindow;
const DownloadManager = require("electron-download-manager");
const {globalShortcut} = require('electron');
const {Menu, MenuItem} = require('electron');
var menu = null;
var playlistMenu = null;
var playlistMenuMusic = null;
var at = {};

if(settings.getSync('General.folder') == undefined)
{
  DownloadManager.register({downloadFolder: app.getPath("downloads") + "/MP3Downloader"});
  settings.setSync('General', {
            'folder': app.getPath("downloads") + "/MP3Downloader"
    });
}
else
  DownloadManager.register({downloadFolder: settings.getSync('General.folder').toString()});

const path = require('path');
const url = require('url');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow () {

   // Create the browser window.
  mainWindow = new BrowserWindow({width: 1220, height: 800, frame: false, minWidth: 1220,});

  //disable default menu bar
  mainWindow.setMenu(null);

  // and load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, '/pages/index.html'),
    protocol: 'file:',
    slashes: true
  }));

  // Open the DevTools.
  //mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  createWindow();     


  //search menu
  menu = new Menu();
  menu.append(new MenuItem({label: 'Add to Queue', click() { sendAddToQueue(); }}));

  menu.append(new MenuItem({label: 'Add to Playlist', submenu: [
    {
      label: 'New Playlist',
      click() { createPlaylist(); }
    },
    {
        type: 'separator'
    }
  ]}));

  createMenuPlaylist();

  menu.append(new MenuItem({type: 'separator'}));
  menu.append(new MenuItem({label: 'Download', click() { sendDownload(); }}));
  
  Menu.setApplicationMenu(menu);

  //playlist menu
  playlistMenuMusic = new Menu();
  playlistMenuMusic.append(new MenuItem({label: 'Remove from playlist', click: (data) => { handleMenuPlaylistMusic(data); }}));
  playlistMenuMusic.append(new MenuItem({type: 'separator'}));
  playlistMenuMusic.append(new MenuItem({label: 'Download', click() { sendDownload(); }}));
  Menu.setApplicationMenu(playlistMenuMusic);

  playlistMenu = new Menu();
  playlistMenu.append(new MenuItem({label: 'Rename', click: (data) => { handleMenuPlaylist(data, true); }}));
  playlistMenu.append(new MenuItem({label: 'Delete', click: (data) => { handleMenuPlaylist(data, false); }}));
  Menu.setApplicationMenu(playlistMenu);


  globalShortcut.register('MediaPlayPause', () => {
      playPause();
  });

  globalShortcut.register('CommandOrControl+P', () => {
      playPause();
  });

  globalShortcut.register('MediaNextTrack', () => {
      forward();
  });

  globalShortcut.register('CommandOrControl+N', () => {
      forward();
  });

  globalShortcut.register('CommandOrControl+B', () => {
      backward();
  });

  globalShortcut.register('MediaPreviousTrack', () => {
      backward();
  });
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
});

app.on('will-quit', () => {
  // Unregister a shortcut.
  globalShortcut.unregister('CommandOrControl+X')

  // Unregister all shortcuts.
  globalShortcut.unregisterAll()
});

const {ipcMain} = require('electron')
var selectedElement = null;


// communications between render and main

ipcMain.on('right-click', (event, data) => {
  selectedElement = data;
  menu.popup();
});

ipcMain.on('right-click-playlist-music', (event, data) => {
  selectedElement = data;
  playlistMenuMusic.popup();
});

ipcMain.on('right-click-playlist', (event, data) => {
  selectedElement = data;
  playlistMenu.popup();
});

ipcMain.on('playlist-rename-done', (event, data) => {
  var oldName = data[0];
  var newName = data[1];
  renamePlaylist(oldName, newName);
});

ipcMain.on('test', (event, data) => {
  createMenuPlaylist();
});

ipcMain.on('getMusicFromPlaylist', (event, data) => {
  getMusicFromPlaylist(data.playlistName);
});

ipcMain.on('deleteFromPlaylist', (event, data) => {
  deleteFromPlaylist(data.playlistName, data.title);
});

ipcMain.on('deletePlaylist', (event, data) => {
  deletePlaylist(data.playlistName);
});


//-------------------------------------

function sendAddToQueue()
{
  mainWindow.webContents.send('addToQueue', selectedElement);
}

function sendDownload()
{
  mainWindow.webContents.send('download', selectedElement);
}
//-----------------------------------

function forward()
{
  mainWindow.webContents.send('forward', null);
}

function backward()
{
  mainWindow.webContents.send('backward', null);
}

function playPause()
{
  mainWindow.webContents.send('playPause', null);
}
// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

// used on startup
function createMenuPlaylist()
{
    var json = getPlaylists();

    if(json == undefined || json == null)
      return;

    for(var i = 0; i < Object.keys(json).length; i++)
    {
      var playlistName = Object.keys(json)[i];
      
      var submenuItem = new MenuItem({label: playlistName, click: (data) => {handleMenu(data)}});

      menu.items[1].submenu.append(submenuItem);

    }
}

//used on creation of new playlist

function handleMenu(data)
{
  addMusicToPlaylist(data.label);
}

function handleMenuPlaylistMusic(data)
{
  deleteFromPlaylist(selectedElement.playlistName, selectedElement.title);
}

function handleMenuPlaylist(data, isRename)
{
  if(isRename)
    mainWindow.webContents.send('playlist-rename', selectedElement);
  else
    deletePlaylist(selectedElement);
}

function getPlaylists()
{
    const settings = require('electron-settings');

    return settings.getSync('Playlists');
}

function createPlaylist()
{

    var playlistNumber = menu.items[1].submenu.items.length-1;   

    var playlistName = "New Playlist " +playlistNumber;

    const settings = require('electron-settings');

    settings.setSync('Playlists.'+playlistName, {});

    var submenuItem = new MenuItem({label: playlistName, click() {addMusicToPlaylist(playlistName)}});

    menu.items[1].submenu.append(submenuItem);

    addMusicToPlaylist(playlistName);
}

function addMusicToPlaylist(playlistName)
{
    var title = selectedElement.title;
    var url = selectedElement.url;
    var artist = selectedElement.author;
    var album = selectedElement.album;
    var duration = selectedElement.duration;
    var albumImg = selectedElement.albumImg;

    const settings = require('electron-settings');

    var json = settings.getSync('Playlists.'+playlistName);
    var lastElementIndex = Object.keys(json).length-1;      
    var lastKey = parseInt((Object.keys(json)[lastElementIndex]))+1;

    if(lastElementIndex < 0)
    {
      lastKey = 0;
    }
      

    settings.setSync('Playlists.'+playlistName+'.'+lastKey, {
            title : title,
            artist: artist,
            album : album,
            duration : duration,
            link : url,
            albumImg : albumImg
    });

    mainWindow.webContents.send('updatePlaylist', playlistName); //send update signal to render
}


function getMusicFromPlaylist(playlistName)
{
    const settings = require('electron-settings');
    
    return settings.getSync('Playlists.'+playlistName);
}

function deletePlaylist(playlistName)
{
    const settings = require('electron-settings');

    settings.deleteSync('Playlists.'+playlistName);

    var submenuItems = menu.items[1].submenu;

    var found = false;

    var i = 0;
    
    while(!found && i <= menu.items[1].submenu.items.length-1)
    {
      if(playlistName === menu.items[1].submenu.items[i].label)
      {
        found = true;
        menu.items[1].submenu.items[i].visible = false; //hide the option until next start
      }
      i++;
    }
    mainWindow.webContents.send('updatePlaylist', null);
}

function deleteFromPlaylist(playlistName, title)
{
    const settings = require('electron-settings');

    var i = 0;
    var found = false;

    var json = settings.getSync('Playlists.'+playlistName);

    while(!found && i <= Object.keys(json).length-1)
    {
        var realKey = Object.keys(json)[i]
        if(json[realKey]['title'] === title)
        {
            found = true;
            settings.deleteSync('Playlists.'+playlistName+'.'+realKey);
        }
        i++;
    }

    mainWindow.webContents.send('updatePlaylist', null);
}

function renamePlaylist(oldName, newName)
{
  const settings = require('electron-settings');
  
  var json = settings.getSync('Playlists.'+oldName);

  settings.deleteSync('Playlists.'+oldName);

  settings.setSync('Playlists.'+newName, json);
}