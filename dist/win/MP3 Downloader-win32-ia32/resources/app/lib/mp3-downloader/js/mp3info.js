//unused

/*function getSpotifyTrackInfoAndPlay(file, title, author, duration, type, queueIndex)
{
    var SpotifyWebApi = require('spotify-web-api-node');

    var path2 = require('path')

    // credentials are optional
    var spotifyApi = new SpotifyWebApi({
        clientId : 'e0d39a60c6414e0ebb1e6fceff253b09',
        clientSecret : '6270d2a72f7d4d7f9a9b29a649b9c415',
    });

    spotifyApi.searchTracks('track:'+title+' artist:'+author+'')
        .then(function(data) {
            try{
                var albumName = data.body.tracks.items[0].album.name;
                var titleResponse = data.body.tracks.items[0].name;
                var artist = data.body.tracks.items[0].artists[0].name;
                var spotifyTrackId = data.body.tracks.items[0].id;
                var spotifyAlbumId = data.body.tracks.items[0].album.id;
                var trackPartialInfo = {album: albumName, title: titleResponse, artist: artist, spotifyTrackId: spotifyTrackId, spotifyAlbumId: spotifyAlbumId, file: file, duration: duration, type: type, queueIndex: queueIndex};
                getAlbumCoverAndPlay(trackPartialInfo);
            }catch(e){
                playWithAlbumCover(file, title, author, 'unknown', duration, type, queueIndex, __dirname.replace(/\\/g,"/") + '/images/no_cover.jpg'); //album not found
            }
        }, function(err) {
            playWithAlbumCover(file, title, author, 'unknown', duration, type, queueIndex, __dirname.replace(/\\/g,"/") + '/images/no_cover.jpg'); //album not found
        });
}

function getAlbumCoverAndPlay(trackPartialInfo)
{
    var SpotifyWebApi = require('spotify-web-api-node');
    var spotifyApi = new SpotifyWebApi({
        clientId : 'e0d39a60c6414e0ebb1e6fceff253b09',
        clientSecret : '6270d2a72f7d4d7f9a9b29a649b9c415',
    });

    spotifyApi.getAlbums([trackPartialInfo.spotifyAlbumId])
        .then(function(data) {
            var albumImgLink = data.body.albums[0].images[2].url;
            var trackInfo = {album: trackPartialInfo.album, title: trackPartialInfo.title, artist: trackPartialInfo.artist, spotifyTrackId: trackPartialInfo.spotifyTrackId, spotifyAlbumId: trackPartialInfo.spotifyAlbumId, file: trackPartialInfo.file, duration: trackPartialInfo.duration, type: trackPartialInfo.type, queueIndex: trackPartialInfo.queueIndex, albumImg: albumImgLink};
            playWithAlbumCover(trackInfo.file, trackInfo.title, trackInfo.artist, trackInfo.album, trackInfo.duration, trackInfo.type, trackInfo.queueIndex, albumImgLink);
        }, function(err) {
            console.error(err);
    });

}*/


//unused
/*
function getSpotifyTrackInfoAndAddToQueue(file, title, author, duration)
{
    var SpotifyWebApi = require('spotify-web-api-node');
    var path2 = require('path')
    // credentials are optional
    var spotifyApi = new SpotifyWebApi({
        clientId : 'e0d39a60c6414e0ebb1e6fceff253b09',
        clientSecret : '6270d2a72f7d4d7f9a9b29a649b9c415',
    });

    spotifyApi.searchTracks('track:'+title+' artist:'+author+'')
        .then(function(data) {
            try{
                var albumName = data.body.tracks.items[0].album.name;
                var titleResponse = data.body.tracks.items[0].name;
                var artist = data.body.tracks.items[0].artists[0].name;
                var spotifyTrackId = data.body.tracks.items[0].id;
                var spotifyAlbumId = data.body.tracks.items[0].album.id;
                var trackPartialInfo = {album: albumName, title: titleResponse, artist: artist, spotifyTrackId: spotifyTrackId, spotifyAlbumId: spotifyAlbumId, file: file, duration: duration};
                getAlbumCoverAndAddToQueue(trackPartialInfo);
            }catch(e){
                addToQueueWithAlbumCover(file, title, author, 'unknown', duration, __dirname.replace(/\\/g,"/") + '/images/no_cover.jpg'); //album not found
            }
        }, function(err) {
            addToQueueWithAlbumCover(file, title, author, 'unknown', duration, __dirname.replace(/\\/g,"/") + '/images/no_cover.jpg'); //album not found
        });
}

function getAlbumCoverAndAddToQueue(trackPartialInfo)
{
    var SpotifyWebApi = require('spotify-web-api-node');
    var spotifyApi = new SpotifyWebApi({
        clientId : 'e0d39a60c6414e0ebb1e6fceff253b09',
        clientSecret : '6270d2a72f7d4d7f9a9b29a649b9c415',
    });

    spotifyApi.getAlbums([trackPartialInfo.spotifyAlbumId])
        .then(function(data) {
            var albumImgLink = data.body.albums[0].images[2].url;
            var trackInfo = {album: trackPartialInfo.album, title: trackPartialInfo.title, artist: trackPartialInfo.artist, spotifyTrackId: trackPartialInfo.spotifyTrackId, spotifyAlbumId: trackPartialInfo.spotifyAlbumId, file: trackPartialInfo.file, duration: trackPartialInfo.duration, albumImg: albumImgLink};
            addToQueueWithAlbumCover(trackInfo.file, trackInfo.title, trackInfo.artist, trackInfo.album, trackInfo.duration, albumImgLink);
        }, function(err) {
            console.error(err);
    });
}*/


function getSpotifyTrackInfo(music, lastIndex)
{
    var SpotifyWebApi = require('spotify-web-api-node');
    var path2 = require('path')
    // credentials are optional
    var spotifyApi = new SpotifyWebApi({
        clientId : 'e0d39a60c6414e0ebb1e6fceff253b09',
        clientSecret : '6270d2a72f7d4d7f9a9b29a649b9c415',
    });

    var j = 0;
    spotifyApi.searchTracks('track:'+music.title+' artist:'+music.author+'')
        .then(function(data) {
            try{
                var albumName = data.body.tracks.items[0].album.name;
                var titleResponse = data.body.tracks.items[0].name;
                var artist = data.body.tracks.items[0].artists[0].name;
                var spotifyTrackId = data.body.tracks.items[0].id;
                var spotifyAlbumId = data.body.tracks.items[0].album.id;
                var trackInfoPartial = {album: albumName, title: titleResponse, artist: artist, spotifyTrackId: spotifyTrackId, spotifyAlbumId: spotifyAlbumId, file: music.downloadURL, duration: music.duration, bitrate: music.bitrate};
                getSpotifyAlbumCover(trackInfoPartial, lastIndex);
         }catch(e){
                //no cover
                var trackInfo = {album: 'unknown',  albumCover: __dirname.replace(/\\/g,"/") + '/images/no_cover.jpg', title: music.title, artist: music.author, spotifyTrackId: 'unknown', spotifyAlbumId: 'unknown', file: music.downloadURL, duration: music.duration, bitrate: music.bitrate, albumImg: __dirname.replace(/\\/g,"/") + '/images/no_cover.jpg', id: musicListWithSpotifyInfo.length};
                musicListWithSpotifyInfo.push(trackInfo);

                if(musicListWithSpotifyInfo.length == lastIndex)
                {   
                    populateList(musicListWithSpotifyInfo);
                    pleerNetMusicList = [];
                    taringaMP3MusicList = [];
                }
         }

        }, function(err) {
            //no cover
            var trackInfo = {album: 'unknown', title: music.title, artist: music.author, spotifyTrackId: 'unknown', spotifyAlbumId: 'unknown', file: music.downloadURL, duration: music.duration, bitrate: music.bitrate, albumImg: __dirname.replace(/\\/g,"/") + '/images/no_cover.jpg', id: musicListWithSpotifyInfo.length};
            musicListWithSpotifyInfo.push(trackInfo);

            if(musicListWithSpotifyInfo.length == lastIndex)
            {   
                populateList(musicListWithSpotifyInfo);
                pleerNetMusicList = [];
                taringaMP3MusicList = [];
            }
        });
}


function getSpotifyAlbumCover(trackPartialInfo, lastIndex)
{   
    var SpotifyWebApi = require('spotify-web-api-node');
    var spotifyApi = new SpotifyWebApi({
        clientId : 'e0d39a60c6414e0ebb1e6fceff253b09',
        clientSecret : '6270d2a72f7d4d7f9a9b29a649b9c415',
    });

    spotifyApi.getAlbums([trackPartialInfo.spotifyAlbumId])
        .then(function(data) {
            var albumImgLink = data.body.albums[0].images[2].url;
            var trackInfo = {album: trackPartialInfo.album, title: trackPartialInfo.title, artist: trackPartialInfo.artist, spotifyTrackId: trackPartialInfo.spotifyTrackId, spotifyAlbumId: trackPartialInfo.spotifyAlbumId, file: trackPartialInfo.file, duration: trackPartialInfo.duration, bitrate: trackPartialInfo.bitrate, albumImg: albumImgLink, id: musicListWithSpotifyInfo.length};
            musicListWithSpotifyInfo.push(trackInfo);

            if(musicListWithSpotifyInfo.length == lastIndex)
            {   
                populateList(musicListWithSpotifyInfo);
                pleerNetMusicList = [];
                taringaMP3MusicList = [];
            }

        }, function(err) {
            console.error(err);
    });
}