function startDownload(url, title, author)
{
    const settings = require('electron-settings');
    var filename = url.substring(url.lastIndexOf('/')+1);
    require("electron").remote.require("electron-download-manager").download({url: url, onProgress(progress){
        updateProgress(progress, filename, title, author);
    }}, function(error, url){
        if(error){
            alert("ERROR: " + url);
            return;
        }
        if(!authorExist(author))
            sendNotifications("Download completed",  title + " was download successfully", "downloadCompleted"); //windows notification?
        else
            sendNotifications("Download completed",  author + " - " + title + " was download successfully", "downloadCompleted"); //windows notification?
    });
}

function updateProgress(progress, filename, title, author)
{
    try
    {
        //already in download
        if(!authorExist(author))
            document.getElementById(filename+'-name').innerHTML = title + ' ('+filename+')';
        else
            document.getElementById(filename+'-name').innerHTML = author + ' - '+ title + ' ('+filename+')';

        var element = document.getElementById(filename+'-progress');
        element.setAttribute('aria-valuenow', parseInt(progress));
        element.setAttribute('style', 'width:'+parseInt(progress)+'%');
        document.getElementById(filename+'-progress').innerHTML = parseInt(progress) + '%';
    }
    catch(e)
    {
        //just started create download bar
        var rowDownload = document.createElement('div');
        rowDownload.setAttribute('class', 'row download');

        var colDownload = document.createElement('div');
        colDownload.setAttribute('class', 'col-xs-12 visible-lg visible-md visible-sm visible-xs');

        var filenameDownload = document.createElement('div');
        filenameDownload.setAttribute('id', filename+'-name');

        var iconFolderOpen = document.createElement('i');
        iconFolderOpen.setAttribute('class', 'fa fa-folder-open openFolderDownloadIcon');
        iconFolderOpen.setAttribute('aria-hidden', 'true');
        iconFolderOpen.setAttribute('onclick', 'openFolder("'+filename+'")')

        var downloadBarDownload = document.createElement('div');
        downloadBarDownload.setAttribute('id', 'downloadBar');
        downloadBarDownload.setAttribute('data-spy', 'affix');
        downloadBarDownload.setAttribute('data-offset-top', '200');
        downloadBarDownload.setAttribute('data-target', '#hide');

        var progressDownload = document.createElement('div');
        progressDownload.setAttribute('class', 'progress');

        var progressBarDownload = document.createElement('div');
        progressBarDownload.setAttribute('id', filename+'-progress');
        progressBarDownload.setAttribute('class', 'progress-bar');
        progressBarDownload.setAttribute('role', 'progressbar');
        progressBarDownload.setAttribute('aria-valuenow', '0');
        progressBarDownload.setAttribute('aria-valuemin', '0');
        progressBarDownload.setAttribute('aria-valuemax', '100');
        progressBarDownload.setAttribute('style', 'width:0%');

        colDownload.appendChild(iconFolderOpen);
        colDownload.appendChild(filenameDownload);
        colDownload.appendChild(downloadBarDownload).appendChild(progressDownload).appendChild(progressBarDownload);
        document.getElementById('download').appendChild(rowDownload).appendChild(colDownload);
    }
}


function clearCompletedDownloads()
{
    $("[aria-valuenow=100]").fadeOut(300, function(){
        $("[aria-valuenow=100]").parent().parent().parent().parent().remove();
    });  
}

function openFolder(filename)
{
    const settings = require('electron-settings');
    const path = require('path');
    const {shell} = require('electron').remote;

    var downloadFolder = settings.getSync('General.folder').toString();
    var file = path.join(downloadFolder, filename);

    shell.showItemInFolder(file);
}