const {ipcRenderer} = require('electron');

function switchTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(tabName).style.display = "block";
    try{
        evt.currentTarget.className += " active";
    }catch(e)
    {
        //first start it is normal
    } 
}

function expandSearchBox()
{
    var isExpanded =  ($("#searchBox-container").attr("expanded") === "true");
    var searchBoxInput = $("#searchBox-input");
    var searchBoxIcon = $("#searchBox-icon");
    var searchBoxIconClose = $("#searchBox-icon-close");
    var searchBoxContainer = $("#searchBox-container");

    //expand search box
    if(!isExpanded)
    {
        $("#searchBox-container").attr("expanded", "true");
        searchBoxInput.css("display", "block");
        searchBoxInput.css("background-color", "rgb(255, 255, 255)")
        searchBoxInput.animate({ width: '+=170' }, 'slow');
        searchBoxIcon.css("position", "absolute").css("color","#b0aeae").css("padding", "10px");
        searchBoxIconClose.animate({left: "180px"}, 600)
        searchBoxIconClose.css("display", "block").css("position", "absolute").css("color","#b0aeae").css("padding", "10px")
        searchBoxContainer.css("padding-top", "8px").css("padding-bottom", "8px");
    }
}

function closeSearchBox()
{
    var searchBoxInput = $("#searchBox-input");
    var searchBoxIcon = $("#searchBox-icon");
    var searchBoxIconClose = $("#searchBox-icon-close");
    var isExpanded =  ($("#searchBox-container").attr("expanded") === "true");

    if(isExpanded)
    {
        $("#searchBox-container").attr("expanded", "false");
        searchBoxInput.animate({ width: '-=170' }, 'slow');
        searchBoxIconClose.animate({left: "0px"}, 600)
        searchBoxIcon.css("color","white");
        searchBoxIconClose.fadeOut(0);
        searchBoxInput.css("background-color", "#2976c1").css("boder-color","rgba(231, 231, 231, 0)");
        searchBoxInput.fadeOut(0, function() {
                $("#searchBox-icon").css("position", "relative").css("padding", "0px");
                $("#searchBox-href").css("padding-top", "15px").css("padding-bottom", "15px");
                $("#searchBox-container").css("padding-top", "0px").css("padding-bottom", "0px");
        });
    }
}

function closeWindow()
{
    const { remote } = require('electron');
    remote.BrowserWindow.getFocusedWindow().close();
}

function minimizeWindow()
{
    const { remote } = require('electron');
    remote.BrowserWindow.getFocusedWindow().minimize();
}

function sendNotifications(title, body, notifType)
{
    var options = [
    {
        title: title,
        body: body
    }];

    if(notifType === 'downloadCompleted')
    {
        var downloadCompleted = new Notification(options[0].title, options[0]);
    }
    else
    {
        var otherNotif = new Notification(options[0].title, options[0]);
    }
     

    downloadCompleted.onclick = () => {
        switchTab(event, "downloads");
    }
}

function authorExist(author)
{
    if(author === 'nd' || author == undefined || author == 'unknown')
        return false;
    else
        return true;
}

function convertToMinutes(duration)
{
    var minutes = Math.floor(duration / 60);
    var seconds = duration - minutes * 60;

    return minutes+':'+('0' + seconds).slice(-2);
}

function convertSeconds(duration)
{
    var date = new Date(null);
    date.setSeconds(duration); // specify value for SECONDS here
    return date.toISOString().substr(11, 8);
}

function removeFirstZeroMinutes(duration)
{
    if(duration.charAt(0) === '0')
        duration = duration.substring(1, duration.length);

    return duration;
}

function createSlider()
{
    $(function() {

        //Store frequently elements in variables
        var slider  = $('#slider'),
            tooltip = $('.tooltip');

        //Hide the Tooltip at first
        tooltip.hide();

        //Call the Slider
        slider.slider({
            //Config
            range: "min",
            min: 0,
            value: 50,
            //orientation:"vertical",

            start: function(event,ui) {
                tooltip.fadeIn('fast');
            },

            //Slider Event
            slide: function(event, ui) { //When the slider is sliding

                var volumeIcon = $('#volume-icon');
                var value  = slider.slider('value'),
                    volume = $('.volume');

                if(ui.value == 0)
                    volumeIcon.attr('class', 'fa fa-volume-off');
                else if(ui.value < 50 )
                    volumeIcon.attr('class', 'fa fa-volume-down');
                else
                    volumeIcon.attr('class', 'fa fa-volume-up');

                if(playerIsActive())
                {
                    ap.volume(ui.value/100); //set volume
                    defaultVolume =  (ui.value/100);
                }
                else
                    defaultVolume = (ui.value/100); //set volume if no music is playing
                

                tooltip.css('left', value).text(ui.value);  //Adjust the tooltip accordingly
            },

            stop: function(event,ui) {
                tooltip.fadeOut('fast');
            },
        });

    });
}

function playerIsActive()
{
    if($('#player-container').attr('active') === 'true')
        return true;
    else
        return false;
}

function openQueue()
{
    if($('.queue-popup').attr('expanded') === 'false')
    {
        $('.queue-popup').attr('expanded', 'true');
        $('.queue-popup').attr('style', 'display:block;');
    }
    else
    {
        $('.queue-popup').attr('expanded', 'false');
        $('.queue-popup').attr('style', 'display:none;');
    }
}

//useless actually
/*function fillPlaylist()
{

    $('#playlist-container').empty(); //empty div before append new

    const settings = require('electron-settings');

    json = settings.getSync('Playlists');

    var playlistContainerDiv = document.getElementById('playlist-container');

    if(json == undefined || json == null)
        return;

    for(var i = 0; i < Object.keys(json).length; i++)
    {
        var playlistName = Object.keys(json)[i];

        var musicKeys = Object.keys(json[playlistName]);

        
        var playlistDiv = document.createElement('div');
        playlistDiv.setAttribute('class', 'playlist');

        if(i != 0)
            playlistDiv.setAttribute('style', 'margin-top: 10px;')

        var playlistNameDiv = document.createElement('div');
        playlistNameDiv.setAttribute('class', 'playlist-name');
        //playlistNameDiv.setAttribute('onclick', 'addPlaylistToQueueAndPlay("'+playlistName+'")');

        var playlistNameSpan = document.createElement('span');
        playlistNameSpan.setAttribute('class', 'playlist-name-span');
        playlistNameSpan.innerHTML = playlistName;

        var playlistPlayIcon = document.createElement('i');
        playlistPlayIcon.setAttribute('class', 'fa fa-play-circle-o');
        playlistPlayIcon.setAttribute('aria-hidden', 'true');
        playlistPlayIcon.setAttribute('onclick', 'addPlaylistToQueueAndPlay("'+playlistName+'")');


        playlistNameDiv.appendChild(playlistNameSpan);
        playlistNameDiv.appendChild(playlistPlayIcon);
        playlistContainerDiv.appendChild(playlistDiv).appendChild(playlistNameDiv);


        var musicListDiv = document.createElement('div');
        musicListDiv.setAttribute('class', 'playlistList '+playlistName.replace(/ /g,"-"));
        
        var musicListUl = document.createElement('ul');
        musicListUl.setAttribute('class', 'playlist-list list-unstyled');


        for(var j = 0; j < Object.keys(musicKeys).length; j++)
        {
            musicFromPlaylist = json[playlistName][musicKeys[j]];

            var musicLi = document.createElement('li');
            musicLi.setAttribute('title', musicFromPlaylist['title']);
            musicLi.setAttribute('artist', musicFromPlaylist['artist']);
            musicLi.setAttribute('album', musicFromPlaylist['album']);
            musicLi.setAttribute('albumImg', musicFromPlaylist['alumbImg']);
            musicLi.setAttribute('link', musicFromPlaylist['link']);
            musicLi.setAttribute('duration', musicFromPlaylist['duration']);

            var spanTitle = document.createElement('span');
            spanTitle.setAttribute('class', 'title-artist-playlist');
            spanTitle.setAttribute('onclick', 'addPlaylistToQueueAndPlay("'+playlistName+'"); ap.setMusic('+j+')'); //add all playlist but then set the first to selected
            spanTitle.innerHTML = musicFromPlaylist['title'] + " - " +  musicFromPlaylist['album'];

            var spanDuration = document.createElement('span');
            spanDuration.setAttribute('class', 'playlist-duration');
            spanDuration.innerHTML = musicFromPlaylist['duration'];

            musicLi.appendChild(spanTitle);
            musicLi.appendChild(spanDuration);
            playlistContainerDiv.appendChild(playlistDiv).appendChild(musicListDiv).appendChild(musicListUl).appendChild(musicLi);
        }
    }
    
    $('.playlist-name-span').click(function(){
        
        var index = parseInt(($(this)).index())+1;

        if($($(this).parent().parent().children()[index]).hasClass('playlistList-expanded'))
        {
        $($(this).parent().parent().children()[index]).fadeOut(400);
        $($(this).parent().parent().children()[index]).removeClass('playlistList-expanded');
        }
        else
        {
        $($(this).parent().parent().children()[index]).fadeIn(400);
        $($(this).parent().parent().children()[index]).addClass('playlistList-expanded');
        }

    });

}*/


/*<div id="playlist">
      <div class="playlist-container">
        <div class="playlist-name">
          <span>Preferiti</span>
        </div>
        <div class="playlistList playlist-1">
          <ul class="playlist-list">
            <li>musica 1</li>
            <li>musica 2</li>
            <li>musica 3</li>
            <li>musica 4</li>
          </ul>
        </div>

        <div class="playlist-name">
          <span>Preferiti 2</span>
        </div>
        <div class="playlistList playlist-2">
          <ul class="playlist-list">
            <li>musica 1</li>
            <li>musica 2</li>
            <li>musica 3</li>
            <li>musica 4</li>
          </ul>
        </div>
      </div>
    </div> */


function fillQueuePopUp(title, author, album, albumImg, index, id)
{
    if(title == null)
        return;
        
    if(index == 0) //if index == 0 is the first song --> empty list
        $('#music-list-popup').empty();

    var musicListPopUp = document.getElementById('music-list-popup');
    var aHrefPlay = document.createElement('a');
    aHrefPlay.setAttribute('onclick', 'playMusicByQueue('+index+',"'+title+'","'+author+'","'+album+'","'+albumImg+'",'+id+')');
    aHrefPlay.setAttribute('href', 'javascript:void(0)');
    var musicEntry = document.createElement('li');
    musicEntry.setAttribute('class', 'queueList');
    if(title.length > 36)
        musicEntry.innerHTML = '<span class="queue-description">' + title.substring(0, 35) +'</span>' + '<br><span class="queue-description" style="font-weight: lighter;">' + author + ' - ' + album + '</span>';
    else
        musicEntry.innerHTML = '<span class="queue-description">' + title +'</span>' + '<br><span class="queue-description" style="font-weight: lighter;">' + author + ' - ' + album + '</span>';
    musicListPopUp.appendChild(aHrefPlay).appendChild(musicEntry);
}

$(window).resize(function()
{
    $('#searchContainer').height(($(window).height() - 130));
    $('#download').height(($(window).height() - 130));
    $('#settingsContainer').height(($(window).height() - 130));
    $('#playlistsContainer').height(($(window).height() - 130));
    $('.definitionsRowPlaylist').width(($(window).width() - 269));
    $('.definitionsRow').width(($(window).width()));
});


function calcTotalTimeQueue(duration, reset)
{
    if(reset)
    {
        $('#actualDuration').attr('duration', '0');
    }

    var actualDuration = parseInt($('#actualDuration').attr('duration')); //duration in seconds

    var time = duration.split(':')  

    var minutesToSeconds = parseInt(time[0])*60;
    var seconds = parseInt(time[1]);
    var actualDuration = actualDuration + minutesToSeconds + seconds;

    $('#actualDuration').attr('duration', actualDuration); //set the new duration in seconds
    $('#actualDuration').html(playerOption.music.length +' tracks , ' +convertSeconds(actualDuration));
}

function getAudioId(string)
{
    return parseInt(string.substring(string.indexOf("-")+1, string.length));
}