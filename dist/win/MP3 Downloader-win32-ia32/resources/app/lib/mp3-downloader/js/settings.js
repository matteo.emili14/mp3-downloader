function selectFolder()
{
    var dialog = require('electron').remote.dialog;

    var folder = dialog.showOpenDialog({properties: ['openDirectory']});
    $("#folder").attr("placeholder", folder[0].toString())
    const settings = require('electron-settings');
    settings.setSync('General', {
            'folder': folder
    });

    restartMessage();
}

function setFolderPlaceHolder()
{
    const settings = require('electron-settings');
    $("#folder").attr("placeholder", settings.getSync('General.folder').toString())
}

function restartMessage()
{
    var dialog = require('electron').remote.dialog;
    dialog.showMessageBox({
        type: 'info',
        title: 'Settings saved',
        buttons: ['OK'],
        message: 'Settings saved, please restart the App',
    });
}

function getAppVersion()
{
    var app = require('electron').remote.app;
    version = app.getVersion();
    $("#info-version").html("App version: "+version);
}