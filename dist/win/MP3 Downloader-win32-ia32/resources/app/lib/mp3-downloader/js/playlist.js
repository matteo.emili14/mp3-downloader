//used in playlist system -> says to main what to do
function getMusicFromPlaylist()
{
    ipcRenderer.send('getMusicFromPlaylist');
}

function deletePlaylist()
{
    ipcRenderer.send('deletePlaylist');
}

// ----------------------------------
function playlistLoader()
{
    const settings = require('electron-settings');

    var playlists = settings.getSync('Playlists');
  
    if(playlists == undefined || playlists == null)
        return;

    var ul = document.getElementById('playlistsContainer').children[0].children[0];
    var playlistContainer = document.getElementById('playlistsContainer');

    //clean playlist list (left)
    while (ul.firstChild) {
        ul.removeChild(ul.firstChild);
    }

    //clean playlist list
    while (playlistContainer.lastChild != playlistContainer.childNodes[1]) {
        playlistContainer.removeChild(playlistContainer.lastChild);
    }

    for(var i = 0; i < Object.keys(playlists).length; i++)
    {
        var playlistName = Object.keys(playlists)[i];

        var playlist = playlists[playlistName];

        //html creation

        var liPlaylistName = document.createElement('li');
        liPlaylistName.setAttribute('class', 'playlists');
        liPlaylistName.setAttribute('playlistname', playlistName);
        var aPlaylistName = document.createElement('a');
        aPlaylistName.setAttribute('href', 'javascript:void(0)');
        aPlaylistName.setAttribute('class', 'playlistTablinks');
        aPlaylistName.setAttribute('onclick', 'switchPlaylist(event, "'+playlistName.replace(/ /g,"-")+'")');
        aPlaylistName.innerHTML = playlistName;

        ul.appendChild(liPlaylistName).appendChild(aPlaylistName); //insert playlist name to left sidebar

        //--------

        var divPlaylistMusic = document.createElement('div');
        divPlaylistMusic.setAttribute('class', 'playlistMusic');

        var divPlaylistTab = document.createElement('div');
        divPlaylistTab.setAttribute('class', 'playlistTab');
        divPlaylistTab.setAttribute('id', playlistName.replace(/ /g,"-"));

        var divDefinitionsRowPlaylist = document.createElement('div');
        divDefinitionsRowPlaylist.setAttribute('class', 'row definitionsRowPlaylist');
        
        var colXs1 = document.createElement('div');
        colXs1.setAttribute('class', 'col-xs-1');
        colXs1.setAttribute('style', 'margin-right: -40px;');

        var colXs4Title = document.createElement('div');
        colXs4Title.setAttribute('class', 'col-xs-4');
        var spanTitle = document.createElement('span');
        spanTitle.innerHTML = 'Title';

        var colXs3Artist = document.createElement('div');
        colXs3Artist.setAttribute('class', 'col-xs-3');
        var spanArtist = document.createElement('span');
        spanArtist.innerHTML = 'Artist';

        var colXs3Album = document.createElement('div');
        colXs3Album.setAttribute('class', 'col-xs-3');
        var spanAlbum = document.createElement('span');
        spanAlbum.innerHTML = 'Album';

        var colXs1Duration = document.createElement('div');
        colXs1Duration.setAttribute('class', 'col-xs-1');
        var spanDuration = document.createElement('span');
        spanDuration.innerHTML = 'Duration';

        divDefinitionsRowPlaylist.appendChild(colXs1);
        divDefinitionsRowPlaylist.appendChild(colXs4Title).appendChild(spanTitle);
        divDefinitionsRowPlaylist.appendChild(colXs3Artist).appendChild(spanArtist);
        divDefinitionsRowPlaylist.appendChild(colXs3Album).appendChild(spanAlbum);
        divDefinitionsRowPlaylist.appendChild(colXs1Duration).appendChild(spanDuration);
        playlistContainer.appendChild(divPlaylistMusic).appendChild(divPlaylistTab).appendChild(divDefinitionsRowPlaylist);

        //--------


        for(var j = 0; j < Object.keys(playlist).length; j++)
        {
            
            var musicIndex = Object.keys(playlist)[j];

            var music = playlist[Object.keys(playlist)[j]];

            var divRow = document.createElement('div');
            divRow.setAttribute('url', music['link']);
            divRow.setAttribute('title', music['title']);
            divRow.setAttribute('author', music['artist']);
            divRow.setAttribute('album', music['album']);
            divRow.setAttribute('duration', music['duration']);
            divRow.setAttribute('albumImg', music['albumImg']);
            divRow.setAttribute('fromPlaylist', playlistName);

            if(j == 0)
                divRow.setAttribute('class', 'row firstPlaylistRow playlistRow rowEven audioId-'+j);
            else if(j % 2 == 0) //pari
                divRow.setAttribute('class', 'row playlistRow rowEven audioId-'+j);
            else
                divRow.setAttribute('class', 'row playlistRow rowOdd audioId-'+j);



            //-------
            var colEqualizer = document.createElement('div');
            colEqualizer.setAttribute('class', 'col-xs-1 bar-c');
            colEqualizer.setAttribute('onclick', '');
            //colEqualizer.setAttribute('style', 'display:none');

            var bar1 = document.createElement('div');
            var bar2 = document.createElement('div');
            var bar3 = document.createElement('div');

            bar1.setAttribute('class', 'bar');
            bar1.setAttribute('id', 'bar-1');

            bar2.setAttribute('class', 'bar');
            bar2.setAttribute('id', 'bar-2');

            bar3.setAttribute('class', 'bar');
            bar3.setAttribute('id', 'bar-3');

            colEqualizer.appendChild(bar1);
            colEqualizer.appendChild(bar2);
            colEqualizer.appendChild(bar3);

            divRow.appendChild(colEqualizer);

            //-------

            var colXs1 = document.createElement('div');
            colXs1.setAttribute('class', 'col-xs-1');
            colXs1.setAttribute('style', 'margin-right: -70px;');

            var colXs4Title = document.createElement('div');
            colXs4Title.setAttribute('class', 'col-xs-4');
            var spanTitle = document.createElement('span');
            spanTitle.innerHTML = music['title'];

            var colXs3Artist = document.createElement('div');
            colXs3Artist.setAttribute('class', 'col-xs-3');
            var spanArtist = document.createElement('span');
            spanArtist.innerHTML = music['artist'];

            var colXs3Album = document.createElement('div');
            colXs3Album.setAttribute('class', 'col-xs-3');
            var spanAlbum = document.createElement('span');
            spanAlbum.innerHTML = music['album'];

            var colXs1Duration = document.createElement('div');
            colXs1Duration.setAttribute('class', 'col-xs-1');
            var spanDuration = document.createElement('span');
            spanDuration.innerHTML = music['duration'];

            //ci sta anche music['link'], music['albumImg']

            divRow.appendChild(colXs1);
            divRow.appendChild(colXs4Title).appendChild(spanTitle);
            divRow.appendChild(colXs3Artist).appendChild(spanArtist);
            divRow.appendChild(colXs3Album).appendChild(spanAlbum);
            divRow.appendChild(colXs1Duration).appendChild(spanDuration);
            playlistContainer.appendChild(divPlaylistMusic).appendChild(divPlaylistTab).appendChild(divRow);
        }
    }

    registerPlaylistClickEvents();
}


function switchPlaylist(evt, playlistName)
{
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("playlistTab");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("playlistTablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(playlistName).style.display = "block";
    try{
        evt.currentTarget.className += " active";
    }catch(e)
    {
        //first start it is normal
    } 
}


function registerPlaylistClickEvents(){

    $('.playlistRow').contextmenu(function()
    {
        var title = $(this).attr('title');
        var author = $(this).attr('author');
        var duration = $(this).attr('duration');
        var url = $(this).attr('url');
        var albumImg = $(this).attr('albumImg');
        var album = $(this).attr('album');
        var id = getAudioId($(this).attr('class'));
        var playlistName = $(this).attr('fromPlaylist');

        var playlistDetails = {title: title, author: author, duration: duration, album: album, albumImg: albumImg, url: url, id: id, playlistName: playlistName};
        ipcRenderer.send('right-click-playlist-music', playlistDetails);
    });

    $('.playlists').contextmenu(function()
    {
        var playlistName = $(this).attr('playlistname');
        ipcRenderer.send('right-click-playlist', playlistName);
    });


    $('.playlistRow').click(function()
    {
        if($('#clickedPlaylist').length && $(this).attr('id') !== 'clickedPlaylist') //check if clicked exists
        {
            $('#clickedPlaylist').removeAttr('style');
            $('#clickedPlaylist').removeAttr('id');
        }

        $(this).attr('style', 'background-color: rgb(41, 119, 193); box-shadow: 0px 0px 0px 1px rgba(41, 118, 193, 0.75) inset; color: #f9f9f9;');
        $(this).attr('id', 'clickedPlaylist');
    });

    $('.playlistRow').dblclick(function(){ //double click event -> play
        setSelected(getAudioId(this.className), true);
    });

    $('.playlists').click(function()
    {
        if($('#selectedPlaylist').length && $(this).attr('id') !== 'selectedPlaylist') //check if clicked exists
        {
            $('#selectedPlaylist').removeAttr('style');
            $($('#selectedPlaylist').firstChild).removeAttr('style');
            $('#selectedPlaylist').removeAttr('id');
        }

        $(this).attr('style', 'background-color: rgba(41, 118, 193, 0.57);');
        $($(this).firstChild).attr('style', "color: #23527c;");
        $(this).attr('id', 'selectedPlaylist');
    });
}

function renamePlaylistSwitchToInput(oldName)
{
    var playlistName = $('[playlistname="'+oldName+'"]');
    playlistName.children(0).remove();
    var playlistInput = document.createElement('input');
    playlistInput.setAttribute('class', 'playlistInput');
    playlistInput.setAttribute('oldName', oldName);
    playlistName.append(playlistInput);

     //use enter to rename
      $(".playlistInput").keyup(function(event){
          if(event.keyCode == 13){
              var newName = ($(".playlistInput").val());
              var oldName = $(".playlistInput").attr('oldname');
              renamePlaylistSwitchA(oldName, newName);
              names = [oldName, newName];
              ipcRenderer.send('playlist-rename-done', names);
          }
      });
}

function renamePlaylistSwitchA(oldName, newName)
{
    var playlistName = $('[playlistname="'+oldName+'"]');
    playlistName.children(0).remove();
    playlistName.attr('playlistname', newName);
    var playlistA = document.createElement('a');
    playlistA.setAttribute('href', 'javascript:void(0)');
    playlistA.setAttribute('class', 'playlistTablinks');
    playlistA.setAttribute('onclick', 'switchPlaylist(event, "'+newName.replace(/ /g,"-")+'")');
    playlistA.innerHTML = newName;
    playlistName.append(playlistA);
    document.getElementById(oldName.replace(/ /g,"-")).setAttribute('id', newName.replace(/ /g,"-"));
}


ipcRenderer.on('updatePlaylist', (event, data) => {
    playlistLoader();
});

ipcRenderer.on('playlist-rename', (event, data) => {
    renamePlaylistSwitchToInput(data);
});