class Music {
    constructor(title, duration, bitrate, dimension, downloadLink)
    {
        this.title = title;
        this.duration = duration;
        this.bitrate = bitrate;
        this.dimension = dimension;
        this.downloadLink = downloadLink;
    }

    get title()
    {
        return this.title;
    }

    get duration()
    {
        return this.duration;
    }

    get bitrate()
    {
        return this.bitrate;
    }

    get dimension()
    {
        return this.dimension;
    }

    get downloadLink()
    {
        return this.downloadLink;
    }


    set title(title)
    {
        this.title = title;
    }

    set duration(duration)
    {
        this.duration = duration;
    }

    set bitrate(bitrate)
    {
        this.bitrate = bitrate;
    }

    set dimension(dimension)
    {
        this.dimension = dimension;
    }

    set downloadLink(downloadLink)
    {
        this.downloadLink = downloadLink;
    }

}