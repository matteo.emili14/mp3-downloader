function switchTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}



function expandSearchBox()
{
    var isExpanded =  ($("#searchBox-container").attr("expanded") === "true");
    var searchBoxInput = $("#searchBox-input");
    var searchBoxIcon = $("#searchBox-icon");
    var searchBoxIconClose = $("#searchBox-icon-close");
    var searchBoxContainer = $("#searchBox-container");

    //expand search box
    if(!isExpanded)
    {
        $("#searchBox-container").attr("expanded", "true");
        searchBoxInput.css("display", "block");
        searchBoxInput.css("background-color", "rgb(255, 255, 255)")
        searchBoxInput.animate({ width: '+=170' }, 'slow');
        searchBoxIcon.css("position", "absolute").css("color","#b0aeae").css("padding", "10px");
        searchBoxIconClose.animate({left: "180px"}, 600)
        searchBoxIconClose.css("display", "block").css("position", "absolute").css("color","#b0aeae").css("padding", "10px")
        searchBoxContainer.css("padding-top", "8px").css("padding-bottom", "8px");
    }
}

function closeSearchBox()
{
    var searchBoxInput = $("#searchBox-input");
    var searchBoxIcon = $("#searchBox-icon");
    var searchBoxIconClose = $("#searchBox-icon-close");
    var isExpanded =  ($("#searchBox-container").attr("expanded") === "true");

    if(isExpanded)
    {
        $("#searchBox-container").attr("expanded", "false");
        searchBoxInput.animate({ width: '-=170' }, 'slow');
        searchBoxIconClose.animate({left: "0px"}, 600)
        searchBoxIcon.css("color","white");
        searchBoxIconClose.fadeOut(500);
        searchBoxInput.css("background-color", "#2976c1")
        searchBoxInput.fadeOut(500, function() {
                $("#searchBox-icon").css("position", "relative").css("padding", "0px");
                $("#searchBox-href").css("padding-top", "15px").css("padding-bottom", "15px");
                $("#searchBox-container").css("padding-top", "0px").css("padding-bottom", "0px");
        });
    }
}


//create settings window
function openSettings() 
{
    // In the main process.
    var {BrowserWindow} = require('electron').remote

    // Or use `remote` from the renderer process.
    // const {BrowserWindow} = require('electron').remote
    

    var win = new BrowserWindow({width: 800, height: 600})
    win.setMenu(null);
    win.on('closed', () => {
        win = null
    })

    win.webContents.openDevTools()

    // Or load a local HTML file
    win.loadURL("file://"+ __dirname+"/settings.html");
}