var ap = {};
var playerOption = {};
var defaultVolume = 0.5; //default volume
var randomMusic = false;

var infoCurrentPlaying = {};


//unused
/*
function play(file, title, author, duration, type, queueIndex)
{
    getSpotifyTrackInfoAndPlay(file, title, author, duration, type, queueIndex);
}*/

function playWithAlbumCover(file, title, author, album, duration, type, queueIndex, albumCover, id)
{
    var path2 = require('path');

    if(type !== 'playlistAlreadyExists')
    {
        playerOption = {
            element: document.getElementById('player1'),                       // Optional, player element
            narrow: false,                                                     // Optional, narrow style
            autoplay: true,                                                    // Optional, autoplay song(s), not supported by mobile browsers
            showlrc: 0,                                                        // Optional, show lrc, can be 0, 1, 2, see: ###With lrc
            mutex: true,                                                       // Optional, pause other players when this player playing
            theme: '#e6d0b2',                                                  // Optional, theme color, default: #b7daff
            mode: 'order',                                                    // Optional, play mode, can be `random` `single` `circulation`(loop) `order`(no loop), default: `circulation`
            preload: 'metadata',                                               // Optional, the way to load music, can be 'none' 'metadata' 'auto', default: 'auto'
            listmaxheight: '513px',                                            // Optional, max height of play list

            //create a temp playlist to use as queue
            music: [{                                                           // Required, music info, see: ###With playlist
                title: title,                                                   // Required, music title
                author: author,                                                 // Required, music author
                url: file,                                                      // Required, music url
                pic:  albumCover,                                                        // Optional, music picture
                lrc: '[00:00.00]lrc here\n[00:01.00]aplayer',                   // Optional, lrc, see: ###With lrc
                duration: duration,
                id: id
            }]
        }
        fillQueuePopUp(title, author, album, albumCover, 0, id) //fill queue with only the file played (is the first so index = 0)
        calcTotalTimeQueue(duration, true);
    }


    const BrowserWindow = require('electron').remote.BrowserWindow;
    const globalShortcut = require('electron').remote.globalShortcut;
    const nativeImage = require('electron').remote.nativeImage;
    const path = require('path');

    var window = BrowserWindow.getAllWindows()[0];

    window.setThumbarButtons([  
        {
            tooltip: 'backward',
            icon: nativeImage.createFromPath(path.join(__dirname, '/images/backward.png')),
            click: function(){ backward();}
        },
        {
            tooltip: 'pause',
            icon: nativeImage.createFromPath(path.join(__dirname, '/images/pause.png')),
            click: function(){ playPauseButton();}
        },
        {
            tooltip: 'forward',
            icon: nativeImage.createFromPath(path.join(__dirname, '/images/forward.png')),
            click: function(){ forward();}
        }
    ]);


    if(!authorExist(author))
    {
        playerOption.music.author = '';
    }

    var APlayer = require('aplayer');
    ap = new APlayer(playerOption);
    
    if(queueIndex != null && queueIndex != undefined)
    {
        ap.setMusic(queueIndex);
    }

    ap.volume(defaultVolume);

    $('#play-navbar').attr('class', 'fa fa-pause'); //play
    $('#player-container').fadeIn(400);
    $('#player-container').attr('active', 'true');

    ap.on('play', function(){

        $(".bar").removeClass("noAnim");

        setSelected(playerOption.music[ap.playIndex].id ,false);
        
        //<-- ELECTRON RELATED 
        //set taskbar icon
        window.setThumbarButtons([  
            {
                tooltip: 'backward',
                icon: nativeImage.createFromPath(path.join(__dirname, '/images/backward.png')),
                click: function(){ backward();}
            },
            {
                tooltip: 'pause',
                icon: nativeImage.createFromPath(path.join(__dirname, '/images/pause.png')),
                click: function(){ playPauseButton();}
            },
            {
                tooltip: 'forward',
                icon: nativeImage.createFromPath(path.join(__dirname, '/images/forward.png')),
                click: function(){ forward();}
            }
        ]);


        $('#play-navbar').attr('class', 'fa fa-pause'); //play;

        if(!playerIsActive())
        {
            $('#player-container').attr('active', 'true');
            $('#player-container').fadeIn(400);
        }

    });

    ap.on('pause', function(){

        $(".bar").addClass("noAnim");

        window.setThumbarButtons([{
                tooltip: 'backward',
                icon: nativeImage.createFromPath(path.join(__dirname, '/images/backward.png')),
                click: function(){ backward();}
            },
            {
                tooltip: 'play',
                icon: nativeImage.createFromPath(path.join(__dirname, '/images/play.png')),
                click: function(){ playPauseButton();}
            },
            {
                tooltip: 'forward',
                icon: nativeImage.createFromPath(path.join(__dirname, '/images/forward.png')),
                click: function(){ forward();}
            }
        ]);

        $('#play-navbar').attr('class', 'fa fa-play');
    });

    ap.on('ended', function(){
        $('#play-navbar').attr('class', 'fa fa-play');
        $('#player-container').fadeOut(400);
        $('#player-container').attr('active', 'false');
    });
}


function playPauseButton()
{
    if(playerIsActive())
        if(ap.audio.paused)
            ap.play();
        else
            ap.pause();
    else
        if(playerOption.music != undefined)
            playWithAlbumCover(null, null, null, null, 'playlistAlreadyExists', null);
}


//unused
/*
function addToQueue(file, title, author, duration)
{
    getSpotifyTrackInfoAndAddToQueue(file, title, author, duration);
}*/

function addToQueueWithAlbumCover(file, title, author, album, duration, albumCover, id)
{
    if(playerIsActive() && playerOption.music != undefined) //check if is playing --> add to queue
    {
        //immagine album
        var music = {                                                       // Required, music info, see: ###With playlist
            title: title,                                                   // Required, music title
            author: author,                                                 // Required, music author
            url: file,                                                      // Required, music url
            pic: albumCover,                                                         // Optional, music picture
            lrc: '[00:00.00]lrc here\n[00:01.00]aplayer',                   // Optional, lrc, see: ###With lrc
            duration: duration,
            id: id
        };
        playerOption.music.push(music);
    }
    else
    {
        if(playerOption.music == undefined) //if player is not running and there isnt nothing in queue -> create a new queue
        {
            //immagine album
            playerOption = {
                element: document.getElementById('player1'),                       // Optional, player element
                narrow: false,                                                     // Optional, narrow style
                autoplay: true,                                                    // Optional, autoplay song(s), not supported by mobile browsers
                showlrc: 0,                                                        // Optional, show lrc, can be 0, 1, 2, see: ###With lrc
                mutex: true,                                                       // Optional, pause other players when this player playing
                theme: '#e6d0b2',                                                  // Optional, theme color, default: #b7daff
                mode: 'order',                                                    // Optional, play mode, can be `random` `single` `circulation`(loop) `order`(no loop), default: `circulation`
                preload: 'metadata',                                               // Optional, the way to load music, can be 'none' 'metadata' 'auto', default: 'auto'
                listmaxheight: '513px',                                            // Optional, max height of play list

                //create a temp playlist to use as queue
                music: [{                                                           // Required, music info, see: ###With playlist
                    title: title,                                                   // Required, music title
                    author: author,                                                 // Required, music author
                    url: file,                                                      // Required, music url
                    pic: albumCover,                                                        // Optional, music picture
                    lrc: '[00:00.00]lrc here\n[00:01.00]aplayer',                    // Optional, lrc, see: ###With lrc
                    duration: duration,
                    id: id
                }]
            };
        }
        else //if player is not running but a queue exists -> add music to existing queue
        {
            //immagine album
            var music = {                                                       // Required, music info, see: ###With playlist
                title: title,                                                   // Required, music title
                author: author,                                                 // Required, music author
                url: file,                                                      // Required, music url
                pic: albumCover,                                                         // Optional, music picture
                lrc: '[00:00.00]lrc here\n[00:01.00]aplayer',                   // Optional, lrc, see: ###With lrc
                duration: duration,
                id: id
            };
            playerOption.music.push(music);
        }
    }

    fillQueuePopUp(title, author, album, albumCover, playerOption.music.length-1, id)
    calcTotalTimeQueue(duration, false);
}

function forward()
{
    if(playerIsActive())
    {
        if(ap.playIndex == playerOption.music.length-1)
            console.log('cant forward, this is the last song!')
        else
        {
            var currentIndex = ap.playIndex
            var newIndex = ap.playIndex +1;
            var listIndex = playerOption.music[newIndex].id;
            ap.setMusic(newIndex);
            setSelected(listIndex, false);
        }
    }
    else
    {
        if(ap.playIndex == playerOption.music.length-1)
            console.log('cant forward, this is the last song!')
        else
        {
            var currentIndex = ap.playIndex
            var newIndex = ap.playIndex +1;;
            var listIndex = playerOption.music[newIndex].id;
            ap.setMusic(newIndex);
            ap.play();
            setSelected(listIndex, false);
        }
    }
}

function backward()
{
    if(playerIsActive())
    {
        if(ap.playIndex == 0)
            console.log('cant go back, this is the first song!')
        else
        {
            var currentIndex = ap.playIndex
            var newIndex = ap.playIndex -1;
            var listIndex = playerOption.music[newIndex].id;
            ap.setMusic(newIndex);
            setSelected(listIndex, false);
        }
    }
    else
    {
        if(ap.playIndex == 0)
            console.log('cant go back, this is the first song!')
        else
        {
            var currentIndex = ap.playIndex
            var newIndex = ap.playIndex -1;
            var listIndex = playerOption.music[newIndex].id;
            ap.setMusic(newIndex);
            ap.play();
            setSelected(listIndex, false);
        }
    }
}

function addAllToQueue(musicList)
{
    //sortedMusicList = []; enable to sort but it is useless actually
    var i = 0;
    while(i < musicList.length)
    {
        if(musicList[i].id == i)
        {
            addToQueueWithAlbumCover(musicList[i].file, musicList[i].title, musicList[i].artist, musicList[i].album, musicList[i].duration, musicList[i].albumImg, musicList[i].id);
            //sortedMusicList.push(musicList[i]); enable to sort but it is useless actually
            i++;
        }
    }
}

function playMusicByQueue(index, title, author, album, albumImg, id)
{
    if(ap.playIndex == undefined)
    {
        var file = playerOption.music[index].url;
        var duration = playerOption.music[index].duration;


        playWithAlbumCover(file, title, author, album, duration, 'playlistAlreadyExists', index, albumImg, id);
        setSelected(id, false);
    }
    else
        ap.setMusic(index);
        setSelected(id, false);
}

function clearQueue()
{
    /*var tempArray = playerOption.music;
    var doNotRemove = tempArray[ap.playIndex];*/
    $('#actualDuration').attr('duration', '0');
    $('#actualDuration').html('');
    $('#music-list-popup').empty(); //empty queue popup
    playerOption.music = []
}

function addPlaylistToQueueAndPlay(playlistName)
{
    const settings = require('electron-settings');

    var musicList = [];

    var musicKeys = Object.keys(json[playlistName]);

    for(var i = 0; i < Object.keys(musicKeys).length; i++)
    {
        json = settings.getSync('Playlists');

        musicFromPlaylist = json[playlistName][musicKeys[i]];

        var music = {file: musicFromPlaylist['link'], title: musicFromPlaylist['title'], artist: musicFromPlaylist['artist'], album: musicFromPlaylist['album'], duration: musicFromPlaylist['duration'], albumImg: musicFromPlaylist['albumImg'], id: i};
    
        musicList.push(music);
    }

    
    if(playerIsActive()) //reinitialize player with playlist
    {
        ap.pause();
        playerOption = [];
        addAllToQueue(musicList);
        playWithAlbumCover(playerOption.music[0].url, playerOption.music[0].title, playerOption.music[0].author, playerOption.music[0].album, playerOption.music[0].duration, 'playlistAlreadyExists', 0, playerOption.music[0].pic, 0); //start playing
    }
    else //initialize player with playlist
    {
        addAllToQueue(musicList); // add to queue
        playWithAlbumCover(playerOption.music[0].url, playerOption.music[0].title, playerOption.music[0].author, playerOption.music[0].album, playerOption.music[0].duration, 'playlistAlreadyExists', 0, playerOption.music[0].pic, 0); //start playing
    }
}

function setPlayerMode(normal, loop, random)
{
    if(ap == undefined)
        return;
        
    if(normal)
    {
        ap.audio.loop = false;
        randomMusic = false;
    }
    else if(loop)
    {
        ap.audio.loop = true;
    }
    else
    {
        randomMusic = true;
    }
}

function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

//multimedia actions from main process
ipcRenderer.on('forward', (event, arg) => {
    forward(); 
});
ipcRenderer.on('backward', (event, arg) => {
    backward(); 
});
ipcRenderer.on('playPause', (event, arg) => {
    playPauseButton(); 
});
// -->
