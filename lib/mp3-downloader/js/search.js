var pleerNetMusicList = [];
var taringaMP3MusicList = [];

var taringaMP3Done = false;
var pleerNetDone = false;

var musicListWithSpotifyInfo = [];

function searchInit(stringSearched)
{
    searchInitPleerNet(stringSearched);
    searchInitTaringaMP3(stringSearched);
}

function searchDone(pleerNet, taringaMP3)
{
    if(pleerNet)
    {
        pleerNetDone = true;
    }
    else
    {
        taringaMP3Done = true;
    }

    if(pleerNetDone && taringaMP3Done)
    {
        musicListWithSpotifyInfo = []; //clean old array before create a new one
        
        var allMusicList = pleerNetMusicList.concat(taringaMP3MusicList);

        for(var i = 0; i < allMusicList.length-1; i++)
        {
            getSpotifyTrackInfo(allMusicList[i], allMusicList.length-1);
        }
            
        //populateList(allMusicList);

        taringaMP3Done = false;
        pleerNetDone = false;

        /*pleerNetMusicList = [];
        taringaMP3MusicList = [];*/ //moved to mp3info.js
    }
}

function saveDataPleerNet(musicList)
{
    //pleerNetMusicList = musicList; did it before check retrieveDownloadUrlPleerNet()
    searchDone(true, false);
}

function saveDataTaringaMP3(musicList)
{
    taringaMP3MusicList = musicList;
    searchDone(false, true);
}

function populateList(allMusicFound)
{

    //add all musics found to queue
    addAllToQueue(allMusicFound);

    //clean search list
    var myNode = document.getElementById("searchContainer");
    while (myNode.childElementCount > 1) {
        if(myNode.firstChild != myNode.lastChild)
            myNode.removeChild(myNode.lastChild);
    }

    //do search
    for(var i = 0; i < allMusicFound.length; i++)
    {
        var row = document.createElement('div')
        row.setAttribute('url', allMusicFound[i].file);
        row.setAttribute('title', allMusicFound[i].title);
        row.setAttribute('author', allMusicFound[i].artist);
        row.setAttribute('album', allMusicFound[i].album);
        row.setAttribute('duration', allMusicFound[i].duration);
        row.setAttribute('albumImg', allMusicFound[i].albumImg);

        if(i == 0)
            row.setAttribute('class', 'row searchRow rowEven firstSearchRow audioId-'+allMusicFound[i].id); //margin from title artist, album duration bitrate bar
        else if(i % 2 == 0) //pari
            row.setAttribute('class', 'row searchRow rowEven audioId-'+allMusicFound[i].id);
        else
            row.setAttribute('class', 'row searchRow rowOdd audioId-'+allMusicFound[i].id);

        //-------
        var colEqualizer = document.createElement('div');
        colEqualizer.setAttribute('class', 'col-xs-1 bar-c');
        colEqualizer.setAttribute('onclick', '');
        //colEqualizer.setAttribute('style', 'display:none');

        var bar1 = document.createElement('div');
        var bar2 = document.createElement('div');
        var bar3 = document.createElement('div');

        bar1.setAttribute('class', 'bar');
        bar1.setAttribute('id', 'bar-1');

        bar2.setAttribute('class', 'bar');
        bar2.setAttribute('id', 'bar-2');

        bar3.setAttribute('class', 'bar');
        bar3.setAttribute('id', 'bar-3');

        colEqualizer.appendChild(bar1);
        colEqualizer.appendChild(bar2);
        colEqualizer.appendChild(bar3);

        row.appendChild(colEqualizer);


        //-------
        var colTitle = document.createElement('div');
        colTitle.setAttribute('class', 'col-xs-3');

        var spanTitle = document.createElement('span');
        if(allMusicFound[i].title.length < 40)
            spanTitle.innerHTML = allMusicFound[i].title;
        else
            spanTitle.innerHTML = allMusicFound[i].title.substring(0, 39) + '...';
          
        colTitle.appendChild(spanTitle);
        row.appendChild(colTitle);


        //-------
        var colArtist = document.createElement('div');
        colArtist.setAttribute('class', 'col-xs-3');

        var spanArtist = document.createElement('span');
        if(allMusicFound[i].artist.length < 25)
            spanArtist.innerHTML = allMusicFound[i].artist;
        else
            spanArtist.innerHTML = allMusicFound[i].artist.substring(0, 24) + '...';;

        colArtist.appendChild(spanArtist);
        row.appendChild(colArtist);

        //-------
        var colAlbum = document.createElement('div');
        colAlbum.setAttribute('class', 'col-xs-3');

        var spanAlbum = document.createElement('span');
        if(allMusicFound[i].album.length < 25)
            spanAlbum.innerHTML = allMusicFound[i].album;
        else
            spanAlbum.innerHTML = allMusicFound[i].album.substring(0, 24) + '...';;

        colAlbum.appendChild(spanAlbum);
        row.appendChild(colAlbum);

        //-------

        var colDuration = document.createElement('div');
        colDuration.setAttribute('class', 'col-xs-1');

        var spanDuration = document.createElement('span');
        spanDuration.innerHTML = allMusicFound[i].duration;
        colDuration.appendChild(spanDuration);
        row.appendChild(colDuration);

        //-------
        var colBitrate = document.createElement('div');
        colBitrate.setAttribute('class', 'col-xs-1');

        var spanBitrate = document.createElement('span');
        spanBitrate.innerHTML = allMusicFound[i].bitrate;
        colBitrate.appendChild(spanBitrate);
        row.appendChild(colBitrate);


        document.getElementById('searchContainer').appendChild(row);
        
    }
    registerClickEvents();
}

//Pleer.net section
/*function test(trackId)
{
    jQuery.ajax({
        url: 'http://api.pleer.com/token.php',
        dataType: 'JSON',
        type: 'POST',
        //: false,
        data : {
             grant_type : 'client_credentials',
             client_id : '854781',
             client_secret : '8PhXaEHY74BPzEK98Zpy'
        },
        success: function(response) {
            jQuery.ajax({
                url: 'http://api.pleer.com/index.php',
                dataType: 'JSON',
                type: 'POST',
                //: false,
                data : {
                    access_token : response.access_token,
                    method : 'tracks_get_download_link',
                    track_id : trackId,
                    reason: 'listen'
                },
                success: function(response2) {
                    console.log(response2['url']);
                },
                error : function(error) {
                    console.log(error);
                }
            });
        },
        error : function(error) {
            token = 'invalid';
            console.log(error);
        }
    });
}*/

function searchInitPleerNet(stringSearched)
{
    auth_o2auth('http://api.pleer.com/token.php', '854781', '8PhXaEHY74BPzEK98Zpy', stringSearched);
}


function auth_o2auth(url, id, key, stringSearched)
{
    jQuery.ajax({
        url: url,
        dataType: 'JSON',
        type: 'POST',
        //: false,
        data : {
             grant_type : 'client_credentials',
             client_id : id,
             client_secret : key
        },
        success: function(response) {
            searchPleerNet(response.access_token, stringSearched);
        },
        error : function(error) {
            token = 'invalid';
            console.log(error);
        }
    });
}

function searchPleerNet(token, stringSearched)
{
    var musicList = [];
    jQuery.ajax({
        url: 'http://api.pleer.com/index.php',
        dataType: 'JSON',
        type: 'POST',
        //: false,
        data : {
             access_token : token,
             method : 'tracks_search',
             query : stringSearched
        },
        success: function(response) {
            askDownloadURLPleerNet(response, token);
        },
        error : function(error) {
            console.log(error);
        }
    });
}

function askDownloadURLPleerNet(data, token)
{
    
    //var musicList = [];
    //var lastKeyIndex = Object.keys(data['tracks']).length -1;
    //var counter = 0;

    for(var i = 0; i < Object.keys(data['tracks']).length; i++)
    {

        if(i != Object.keys(data['tracks']).length-1)
            retrieveDownloadUrlPleerNet(data['tracks'][Object.keys(data['tracks'])[i]]['id'], i, data, token, false)
        else
            retrieveDownloadUrlPleerNet(data['tracks'][Object.keys(data['tracks'])[i]]['id'], i, data, token, true)


        /*jQuery.ajax({
            url: 'http://api.pleer.com/index.php',
            dataType: 'JSON',
            type: 'POST',
            //: false,
            data : {
                access_token : token,
                method : 'tracks_get_download_link',
                track_id : data['tracks'][Object.keys(data['tracks'])[i]]['id'],
                reason : 'listen',
                beforeSend: test(data['tracks'][Object.keys(data['tracks'])[i]]['id'])
            },
            success: function(response) {
                console.log(response['url']);
                var keyReal =  Object.keys(data['tracks'])[(counter)];
                var title = data['tracks'][keyReal]['track'];
                var author = data['tracks'][keyReal]['artist'];
                var duration = convertToMinutes(data['tracks'][keyReal]['lenght']);
                var bitrate = data['tracks'][keyReal]['bitrate'];
                var downloadURL = response['url'];
                var music = {downloadURL: downloadURL, title: title, author: author, duration: duration, bitrate: bitrate}
                musicList.push(music);
                counter++;
                if(counter == i)
                    saveDataPleerNet(musicList);
            },
            error : function(error) {
                console.log(error);
            }
        });*/
    }
}

function retrieveDownloadUrlPleerNet(trackId, index, data, token, isLastMusic)
{
    jQuery.ajax({
        url: 'http://api.pleer.com/index.php',
        dataType: 'JSON',
        type: 'POST',
        //: false,
        data : {
            access_token : token,
            method : 'tracks_get_download_link',
            track_id : trackId,
            reason: 'listen'
        },
        success: function(response) {
            var keyReal =  Object.keys(data['tracks'])[(index)];
            var title = data['tracks'][keyReal]['track'];
            var author = data['tracks'][keyReal]['artist'];
            var duration = convertToMinutes(data['tracks'][keyReal]['lenght']);
            var bitrate = data['tracks'][keyReal]['bitrate'];
            var downloadURL = response['url'];
            var music = {downloadURL: downloadURL, title: title, author: author, duration: duration, bitrate: bitrate}
            pleerNetMusicList.push(music);
            if(isLastMusic)
               saveDataPleerNet(pleerNetMusicList); 
        },
        error : function(error) {
            console.log(error);
        }
    });
}



// TARINGA SECTION

function searchInitTaringaMP3(stringSearched)
{
    searchTaringaMP3(stringSearched);
}

function searchTaringaMP3(stringSearched)
{
    jQuery.ajax({
        url: 'http://taringamp3.com/mp3/'+stringSearched.replace(/ /g,"-"),
        type: 'POST',
        dataType: 'html',
        //: false,
        success: function(response){
            var musicList = parseResultTaringaMP3(response);
            saveDataTaringaMP3(musicList);
        }
    });
}


function parseResultTaringaMP3(result)
{
    var doc = document.implementation.createDocumentType( 'html', '', '');
    var dom = document.implementation.createDocument('', 'html', doc);

    var body = document.createElement('body');
    body.innerHTML = result;
    dom.documentElement.appendChild(body);

    var musicList = [];
    for (i = 0; i < dom.getElementsByClassName('cplayer-sound-item').length; i++)
    {
        var downloadURL = "http://cs1.mp3.pm/download/"+ dom.getElementsByClassName('cplayer-sound-item')[i].getAttribute('data-download-url');
        var title = dom.getElementsByClassName('cplayer-data-sound-title')[i].innerHTML;
        var duration = removeFirstZeroMinutes(dom.getElementsByClassName('cplayer-data-sound-time')[i].innerHTML);
        var bitrate = 'unknown';
        var author = 'unknown';
        var music = {downloadURL: downloadURL, title: title, author: author, duration: duration, bitrate: bitrate};
        musicList.push(music);
    }

    return musicList;
}

function setSelected(index, newPlayer)
{
    /*if(element == undefined) // not playing by search
        return;*/

    var element = $('.audioId-'+index);
    var title = $(element).attr('title');
    var author = $(element).attr('author');
    var duration = $(element).attr('duration');
    var url = $(element).attr('url');
    var albumImg = $(element).attr('albumImg');
    var album = $(element).attr('album');

    var barContainer = $(element[0].childNodes[0]);

    //do always
    $(".bar").removeClass("noAnim");

    if($('#equalizerActive').length > 0)
    {
        $($('#equalizerActive').children()[0]).fadeOut(0);
        $($('#equalizerActive').children()[1]).fadeOut(0);
        $($('#equalizerActive').children()[2]).fadeOut(0);

        $('#equalizerActive').removeAttr('id');
    }

    var bar1 = $(element[0].childNodes[0].childNodes[0]);
    var bar2 = $(element[0].childNodes[0].childNodes[1]);
    var bar3 = $(element[0].childNodes[0].childNodes[2]);

    $(bar1).fadeIn(400);
    $(bar2).fadeIn(400);
    $(bar3).fadeIn(400);
    $(barContainer[0]).attr('id', 'equalizerActive');

    if(newPlayer)
    {
        if((title.length + author.length) < 42)
        {
            playWithAlbumCover(url, title, author, album, duration, null, null, albumImg, index); 
            $(".bar").removeClass("noAnim"); //must be placed here because ap.on('play') doesnt work on first just started audio
        }
        else
            if(author.length > 15)
            {
                playWithAlbumCover(url, title.substring(0,25)+"...", author.substring(0,10)+"...", album, duration, null, null, albumImg, index);
                $(".bar").removeClass("noAnim"); //must be placed here because ap.on('play') doesnt work on first just started audio
            }
            else
            {
                playWithAlbumCover(url, title.substring(0,25)+"...", author, album, duration, null, null, albumImg, index);
                $(".bar").removeClass("noAnim"); //must be placed here because ap.on('play') doesnt work on first just started audio
            }
    }
    else
        $(".bar").removeClass("noAnim"); //must be placed here because ap.on('play') doesnt work on first just started audio

}

function registerClickEvents(){

    $('.searchRow').contextmenu(function()
    {
        var title = $(this).attr('title');
        var author = $(this).attr('author');
        var duration = $(this).attr('duration');
        var url = $(this).attr('url');
        var albumImg = $(this).attr('albumImg');
        var album = $(this).attr('album');
        var id = getAudioId($(this).attr('class'));

        var musicDetails = {title: title, author: author, duration: duration, album: album, albumImg: albumImg, url: url, id: id};
        ipcRenderer.send('right-click', musicDetails);
    });


    $('.searchRow').click(function()
    {
        if($('#clicked').length && $(this).attr('id') !== 'clicked') //check if clicked exists
        {
            $('#clicked').removeAttr('style');
            $('#clicked').removeAttr('id');
        }

        $(this).attr('style', 'background-color: rgb(41, 119, 193); box-shadow: 0px 0px 0px 1px rgba(41, 118, 193, 0.75) inset; color: #f9f9f9;');
        $(this).attr('id', 'clicked');
    });

    $('.searchRow').dblclick(function(){ //double click event -> play
        setSelected(getAudioId(this.className), true);
    });
}

ipcRenderer.on('addToQueue', (event, data) => {
    if((data.title.length + data.author.length) < 42) 
        addToQueueWithAlbumCover(data.url, data.title, data.author, data.album, data.duration, data.albumImg, data.id);
    else
        if(data.author.length > 15)
            addToQueueWithAlbumCover(data.url, data.title.substring(0,25)+"...", data.author.substring(0,10)+"...", data.album, data.duration, data.albumImg, data.id);
        else
            addToQueueWithAlbumCover(data.url, data.title.substring(0,25)+"...", data.author, data.album, data.duration, data.albumImg, data.id);
});

ipcRenderer.on('download', (event, data) => {
    startDownload(data.url, data.title, data.author);
});